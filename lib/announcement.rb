# frozen_string_literal: true

require_relative 'announcement/basic_announcement'
require_relative 'announcement/anniversary'
require_relative 'announcement/joining'
require_relative 'announcement/missing_details'
