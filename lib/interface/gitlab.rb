# frozen_string_literal: true

require 'date'
require 'gitlab'

module PeopleOps
  module Interface
    class GitLab
      API_URL = ENV['CI_API_V4_URL'] || 'https://gitlab.com/api/v4'

      def initialize
        @client = Gitlab.client(endpoint: API_URL, private_token: ENV['GITLAB_API_TOKEN'])
      end

      def find_gitlabber(query)
        return unless query

        @client.group_members('gitlab-com', query: query).first
      end

      def create_issue(project, title, options = {})
        @client.create_issue(project, title, options)
      end

      def create_issue_note(project, id, text)
        @client.create_issue_note(project, id, text)
      end

      def get_onboarding_issues(project, args)
        @client.issues(project, args).auto_paginate
      end
    end
  end
end
