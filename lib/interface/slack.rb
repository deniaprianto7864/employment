# frozen_string_literal: true

require 'slack-ruby-client'

module PeopleOps
  module Interface
    class Slack
      def initialize
        ::Slack.configure do |config|
          config.token = ENV['SLACK_API_TOKEN']
        end
        @client = ::Slack::Web::Client.new
      end

      def send_message(channel, message)
        @client.chat_postMessage(channel: channel, text: message, as_user: true)
      end

      def find_user(email)
        @client.users_lookupByEmail(email: email)
      rescue ::Slack::Web::Api::Errors::SlackError
        nil
      end
    end
  end
end
