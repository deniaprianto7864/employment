# frozen_string_literal: true

require 'bamboozled'

module PeopleOps
  module Interface
    class Bamboo
      attr_reader :employees

      def initialize
        @client = Bamboozled.client(subdomain: 'gitlab', api_key: ENV['BAMBOO_API_KEY'])
        @employees = @client.report.custom(:all, 'JSON').select { |employee| employee['flsaCode'] == 'Active' }
      end

      def get_employee_details(id)
        @employees.find { |emp| emp['id'] == id }
      end

      def get_employee(id)
        @client.employee.find(id, %w[firstName lastName jobTitle supervisor hireDate country location department division workEmail])
      end

      def search_employee(name)
        return if name.empty?

        @employees.find { |emp| [emp['displayName'], "#{emp['firstName']} #{emp['lastName']}"].include?(name) }
      end
    end
  end
end
