# frozen_string_literal: true

require_relative 'interface/bamboo'
require_relative 'interface/gitlab'
require_relative 'interface/slack'
