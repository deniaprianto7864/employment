# frozen_string_literal: true

require 'ordinalize'

module PeopleOps
  module Announcement
    class Anniversary < BasicAnnouncement
      def initialize
        @employees = bamboo.employees
      end

      def channel
        @channel ||= ENV['CELEBRATIONS_SLACK_CHANNEL']
      end

      def employee_anniversaries
        @weekstart = PeopleOps::Utils.date_of_prev('Sunday')
        @weekend = @weekstart + 6

        employees = []
        @employees.each do |employee|
          hiredate = Date.parse(employee['hireDate'])
          years = ((@weekend - hiredate) / 365).floor

          next unless years.positive?

          anniversary_this_year = hiredate.next_year(years)

          employees << [employee, years] if anniversary_this_year.between?(@weekstart, @weekend)
        rescue RuntimeError, ArgumentError, TypeError
          next
        end

        employees.sort_by(&:last).reverse
      end

      def message
        messages = []
        employee_anniversaries.each do |employee, years|
          email = employee['workEmail']
          begin
            raise if ENV['DRY_RUN']

            slack_id = slack.find_user(email)['user']['id']
            salutation = "<@#{slack_id}>"
          rescue StandardError
            salutation = "#{employee['firstName']} #{employee['lastName']}"
          end

          message = "• #{salutation}: #{years.ordinalize.capitalize}"
          messages << message
        end
        intro_message = "Congratulations to all GitLab Team Members with Work Anniversaries this week (#{@weekstart} to #{@weekend}). :gitlab: :tada: :dancingpenguin: :all_the_things: \n"

        message = intro_message + messages.join("\n") unless messages.empty?
        message
      end
    end
  end
end
