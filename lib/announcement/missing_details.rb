# frozen_string_literal: true

module PeopleOps
  module Announcement
    class MissingDetails < BasicAnnouncement
      def initialize
        @employees = bamboo.employees
        @issue_fetcher = PeopleOps::Onboarding::IssueFetcher.new
        @required_fields = %w[hireDate firstName lastName workEmail jobTitle supervisor department division country].freeze
        @bamboo_url = 'https://gitlab.bamboohr.com/employees/employee.php?id='
      end

      def channel
        @channel ||= ENV['REMINDER_SLACK_CHANNEL']
      end

      def missing_details_employees
        next_sunday = PeopleOps::Utils.date_of_next('Sunday')
        following_saturday = next_sunday + 6
        employees = []

        @employees.each do |employee|
          hiredate = Date.parse(employee['hireDate'])

          next unless hiredate.between?(next_sunday, following_saturday)

          missing_fields = @required_fields.select { |field| employee[field].nil? || employee[field].empty? }
          employees << [employee, missing_fields] unless missing_fields.empty?
        rescue RuntimeError, ArgumentError, TypeError
          next
        end
        employees
      end

      def message
        new_hires = missing_details_employees
        messages = []

        new_hires.each_with_index do |item, index|
          employee, missing_fields = item
          name = "#{employee['firstName']} #{employee['lastName']}"
          bamboo_id = employee['id']

          msg_string = "#{index + 1}. #{name}: #{@bamboo_url}#{bamboo_id}. Missing fields are #{missing_fields.join(', ')}"

          messages << msg_string
        end

        if messages.empty?
          message = 'Woohoo. BambooHR profile of all team members joining next week are complete. :dancing_banana: '
        else
          intro_message = "The following team members will be joining next week, but have incomplete BambooHR profile. Please take a look. \n"
          message = intro_message + messages.join("\n")
        end

        message
      end
    end
  end
end
