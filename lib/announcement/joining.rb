# frozen_string_literal: true

module PeopleOps
  module Announcement
    class Joining < BasicAnnouncement
      def initialize
        @employees = bamboo.employees
        @issue_fetcher = PeopleOps::Onboarding::IssueFetcher.new
      end

      def channel
        @channel ||= ENV['ANNOUNCEMENT_SLACK_CHANNEL']
      end

      def new_hires
        next_sunday = PeopleOps::Utils.date_of_next('Sunday')
        following_saturday = next_sunday + 6
        employees = []
        @employees.each do |employee|
          hiredate = Date.parse(employee['hireDate'])
          employees << employee if hiredate.between?(next_sunday, following_saturday)
        rescue RuntimeError, ArgumentError, TypeError
          next
        end
        employees.sort_by { |emp| emp['hireDate'] }
      end

      def message
        messages = []

        new_hires.each_with_index do |employee, index|
          name = "#{employee['firstName']} #{employee['lastName']}"
          date = employee['hireDate']
          job = employee['jobTitle']
          email = employee['workEmail']

          msg_string = "#{index + 1}. #{name}"
          msg_string += ", #{email}" unless email.nil?
          msg_string += ", joining on #{date} as #{job}"

          messages << msg_string
        end

        intro_message = "Welcome our newest GitLab Team Members, who are starting next week! Send them some welcome emails or coffee chats! :tada: :gitlab: :coffee: :grin: \n"
        graph_message = "\n\nFor a detailed breakdown and overview of our hiring progress over time, visit https://app.periscopedata.com/app/gitlab/503779"

        message = intro_message + messages.join("\n") + graph_message unless messages.empty?
        message
      end
    end
  end
end
