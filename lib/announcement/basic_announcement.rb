# frozen_string_literal: true

module PeopleOps
  module Announcement
    class BasicAnnouncement
      def bamboo
        @bamboo ||= PeopleOps::Interface::Bamboo.new
      end

      def gitlab
        @gitlab ||= PeopleOps::Interface::GitLab.new
      end

      def slack
        @slack ||= PeopleOps::Interface::Slack.new
      end

      def channel
        raise NotImplementedError
      end

      def message
        raise NotImplementedError
      end

      def announce
        if ENV['DRY_RUN']
          puts message
        else
          return if channel.nil? || message.nil?

          slack.send_message(channel, message)
        end
      end
    end
  end
end
