# frozen_string_literal: true

module PeopleOps
  module Onboarding
    class BasicOnboarding
      attr_accessor :name, :job, :manager, :startdate, :country, :tasks, :bamboo_id, :description

      def project_path
        @project_path ||= ENV['EMPLOYMENT_PROJECT_PATH'] || ENV['CI_PROJECT_PATH']
      end

      def bamboo
        @bamboo ||= PeopleOps::Interface::Bamboo.new
      end

      def gitlab
        @gitlab ||= PeopleOps::Interface::GitLab.new
      end

      def populate_employee_details
        employee = bamboo.get_employee_details(@bamboo_id)

        @name = [employee['firstName'], employee['lastName']].join(' ')
        @job = employee['jobTitle']
        @startdate = employee['hireDate']
        @country = employee['country']
        @entity = employee['location']&.gsub('-', ' ')&.partition(' ')&.first&.downcase
        @department = employee['department']
        @division = employee['division']

        return unless employee['supervisorEId']

        supervisor_name = employee['supervisor']
        supervisor_email = bamboo.get_employee_details(employee['supervisorEId'])['workEmail']

        # Attempt to get GitLab user from name. If not possible, try email.
        @manager = gitlab.find_gitlabber(supervisor_name) || gitlab.find_gitlabber(supervisor_email)
      end
    end
  end
end
