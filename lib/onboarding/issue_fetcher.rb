# frozen_string_literal: true

require_relative 'basic_onboarding'
require 'date'

module PeopleOps
  module Onboarding
    class IssueFetcher < BasicOnboarding
      ONBOARDING_ISSUE_REGEX = /(Onboarding )?(?<name>.*?)('s onboarding)?(,)? starting.* (?<date>\d{4}-\d{2}-\d{2}).* as( a)? (?<job>.*)/i.freeze
      BAMBOO_ID_REGEX = /BAMBOOHR_ID: (?<bamboo_id>\d+)/.freeze

      def initialize; end

      def list_new_hires(args)
        from_date = args[:from_date]
        to_date = args[:to_date]
        on_date = args[:date]

        issue_options = {
          per_page: 100,
          labels: ['onboarding'],
          created_after: Date.today - 90
        }

        issues = gitlab.get_onboarding_issues(project_path, issue_options)
        matching_issues = []

        issues.each do |issue|
          match = issue.title.match(ONBOARDING_ISSUE_REGEX)
          next unless match

          name = match[:name].chomp(',').chomp(':')
          date = Date.parse(match[:date].chomp(','))

          # If a date is specified and issue date does not match that, skip.
          # If a specific date is not passed, check if issue date falls between
          # start and end. If not, skip.
          if on_date
            next unless date == on_date
          else
            next unless date.between?(from_date, to_date)
          end

          job = match[:job].chomp(',').chomp(':')
          id_match = issue.description.match(BAMBOO_ID_REGEX)
          bamboo_id = id_match[:bamboo_id] if id_match

          item = { name: name, date: date, job: job, issue_url: issue.web_url, bamboo_id: bamboo_id }
          matching_issues << item
        end

        matching_issues.sort_by { |item| item[:date] }
      end

      def print_new_hire_list(options)
        employee_issues = list_new_hires(options)
        if employee_issues.empty?
          puts 'Nope. No one is starting on dates matching specified criteria.'
        else
          puts ''
          employee_issues.each_with_index do |employee, index|
            emp_details = [
              employee[:name],
              employee[:date],
              employee[:job],
              employee[:issue_url]
            ]
            emp_details << employee[:bamboo_id] if employee[:bamboo_id]
            msg_string = emp_details.join(', ')

            puts "#{index + 1}. #{msg_string}"
          end
        end
      end
    end
  end
end
