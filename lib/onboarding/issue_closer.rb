# frozen_string_literal: true

require_relative 'basic_onboarding'
require 'date'

module PeopleOps
  module Onboarding
    class IssueCloser < BasicOnboarding
      CLOSE_MESSAGE = <<~MSG
        This onboarding issue has been open for more than 60 days. It is being automatically closed to de-clutter this issue tracker.

        If you have any onboarding tasks remaining, reopen the issue and ping your Manager and PeopleOps to get them completed as soon as possible.

        /close
      MSG

      def initialize; end

      def outdated_issues
        args = {
          per_page: 100,
          labels: ['onboarding'],
          confidential: true,
          state: 'opened',
          created_before: Date.today - 60,
          created_after: Date.today - 67
        }

        gitlab.get_onboarding_issues(project_path, args)
      end

      def close_outdated_issues
        outdated_issues.each do |issue|
          puts [issue.web_url, Date.parse(issue.created_at).strftime('%Y-%m-%d')].join(', ')

          gitlab.create_issue_note(project_path, issue.iid, CLOSE_MESSAGE) unless ENV['DRY_RUN']
        end
      end
    end
  end
end
