# frozen_string_literal: true

require_relative 'basic_onboarding'

module PeopleOps
  module Onboarding
    class IssueCreator < BasicOnboarding
      def generate_description
        @description = File.read('.gitlab/issue_templates/onboarding.md')
        @description.sub!('`__MANAGER_HANDLE__`', "@#{@manager.username}") if @manager
        @description.sub!('`__PEOPLEOPS_HANDLE__`', "@#{ENV['GITLAB_USER_LOGIN']}") if ENV.key?('GITLAB_USER_LOGIN')

        # TODO: Many of these methods follow the same pattern, and could be
        # DRYed
        insert_country_tasks
        insert_entity_tasks
        insert_division_tasks
        insert_department_tasks
      end

      def insert_country_tasks
        insert_country_specific_before_starting
        insert_country_specific_day_one
      end

      def insert_country_specific_before_starting
        country_before_starting = ''
        country_code = @country&.downcase&.gsub(' ', '_')
        country_before_starting_file = ".gitlab/issue_templates/onboarding_tasks/before_starting_#{country_code}.md"

        country_before_starting = File.read(country_before_starting_file) if File.exist?(country_before_starting_file)

        @description.sub!('<!-- include: before_starting_country -->', country_before_starting)
      end

      def insert_country_specific_day_one
        country_tasks = ''
        country_code = @country&.downcase&.gsub(' ', '_')
        country_tasks_file = ".gitlab/issue_templates/onboarding_tasks/country_#{country_code}.md"

        country_tasks = File.read(country_tasks_file) if File.exist?(country_tasks_file)

        @description.sub!('<!-- include: country_tasks -->', country_tasks)
      end

      def insert_entity_tasks
        entity_tasks = ''
        entity_tasks_file = ".gitlab/issue_templates/onboarding_tasks/entity_#{@entity}.md"

        entity_tasks = File.read(entity_tasks_file) if File.exist?(entity_tasks_file)

        @description.sub!('<!-- include: entity_tasks -->', entity_tasks)
      end

      def insert_department_tasks
        return @description unless @department

        department_tasks = ''
        department_slug = @department.gsub(' ', '_').downcase
        department_tasks_file = ".gitlab/issue_templates/onboarding_tasks/department_#{department_slug}.md"

        department_tasks = File.read(department_tasks_file) if File.exist?(department_tasks_file)

        @description.sub!('<!-- include: department_tasks -->', department_tasks)
      end

      def insert_division_tasks
        return @description unless @division

        division_tasks = ''
        division_slug = @division.gsub(' ', '_').downcase
        division_tasks_file = ".gitlab/issue_templates/onboarding_tasks/division_#{division_slug}.md"

        division_tasks = File.read(division_tasks_file) if File.exist?(division_tasks_file)

        @description.sub!('<!-- include: division_tasks -->', division_tasks)
      end

      def create_issue(id)
        @bamboo_id = id
        populate_employee_details
        generate_description

        employee_issue_title = "#{@name}'s onboarding, starting on #{@startdate} as #{@job}"
        bamboo_id_text = "<!-- BAMBOOHR_ID: #{@bamboo_id} -->\n"
        description = bamboo_id_text + @description
        issue_args = {
          description: description,
          labels: ['onboarding'],
          confidential: true,
          due_date: (Date.today + 35).to_s
        }
        assignees = [ENV['GITLAB_USER_ID']]
        assignees << @manager.id if @manager
        issue_args[:assignee_ids] = assignees

        if ENV['DRY_RUN']
          puts employee_issue_title
          puts description
          puts issue_args
        else
          issue = gitlab.create_issue(project_path, employee_issue_title, issue_args)
          puts "Onboarding issue opened at #{issue.web_url}"
        end
      end
    end
  end
end
