# frozen_string_literal: true

module PeopleOps
  class Utils
    class << self
      def date_of_next(day)
        closest_instance = Date.parse(day)
        delta = closest_instance > Date.today ? 0 : 7
        closest_instance + delta
      end

      def date_of_prev(day)
        closest_instance = Date.parse(day)
        delta = closest_instance < Date.today ? 0 : 7
        closest_instance - delta
      end
    end
  end
end
