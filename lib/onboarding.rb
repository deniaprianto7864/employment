# frozen_string_literal: true

require_relative 'onboarding/issue_creator'
require_relative 'onboarding/issue_fetcher'
require_relative 'onboarding/issue_closer'
