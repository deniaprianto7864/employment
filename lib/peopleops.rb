# frozen_string_literal: true

require_relative 'onboarding'
require_relative 'announcement'
require_relative 'interface'
require_relative 'util'

require 'shellwords'
require 'optionparser'
require 'date'

module PeopleOps
  class << self
    def restricted_output
      puts "section_start:#{Time.now.to_i}:chat_reply\r\033[0K"
      yield
    ensure
      puts "section_end:#{Time.now.to_i}:chat_reply\r\033[0K"
    end

    def run(command, args)
      PeopleOps.send(command, args)
    end

    def onboarding(args)
      id = args
      PeopleOps::Onboarding::IssueCreator.new.create_issue(id)
    end

    def listnewhires(args)
      options = {
        from_date: Date.today,
        to_date: Date.today + 7,
        date: nil
      }

      parser = OptionParser.new do |p|
        p.on('-f', '--from fromdate') do |d|
          options[:from_date] = Date.parse(d)
          options[:to_date] = options[:from_date] + 7
        end
        p.on('-t', '--to todate') do |d|
          options[:to_date] = Date.parse(d)
        end
        p.on('-d', '--date date') do |d|
          options[:date] = Date.parse(d)
        end
      end

      arguments = Shellwords.split(args)
      parser.parse!(arguments)

      PeopleOps::Onboarding::IssueFetcher.new.print_new_hire_list(options)
    end

    def joiningannouncement(_args)
      PeopleOps::Announcement::Joining.new.announce
    end

    def anniversaryannouncement(_args)
      PeopleOps::Announcement::Anniversary.new.announce
    end

    def missingdetailsreminder(_args)
      PeopleOps::Announcement::MissingDetails.new.announce
    end

    def closeoutdatedissues(_args)
      PeopleOps::Onboarding::IssueCloser.new.close_outdated_issues
    end
  end
end
