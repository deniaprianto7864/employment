#### For Finance Only

<details>
<summary>Finance</summary>

1. [ ] Finance: Add to Comerica (as user or viewer only if in Finance).
1. [ ] Finance: Add *Employee Record* in NetSuite with department classification.
1. [ ] Finance: Add to Carta with department classification.

</details>