### For employees in India only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Read through the [India Specific Benefits](https://about.gitlab.com/handbook/benefits/#specific-to-india-based-employees). This will explain what is available. If you have any questions please contact Lyra HR at +91 80 40408181 or email them at hr@lyrainfo.com. Lyra HR will also reach out to you in the first week of starting at GitLab to complete their onboarding documents for payroll (if this has not already been done during the contract signing stage).

</details>

<details>
<summary>People Ops</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has two entries marked as `Probation Period` with date as the hire date and `probation period ends` with the end date as exactly 6 months from the hire date.
1. [ ] People Ops: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>