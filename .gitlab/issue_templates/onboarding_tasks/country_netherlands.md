### For employees in the Netherlands only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Fill in the payroll information [form](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSekAouLh-DQsDXVho4TYL62CsBK8Pj0NhfI--npa3L1nF8IqA/viewform).
This info is needed to get the team member's profile ready with Savvy HR in order to get pay slips and other information. You will also need to provide a copy of your residence permit (if applicable), copy of your debit card, and passport. You can send these to People Operations separately.
1. [ ] New Team Member: Fill in the [wage tax form](https://drive.google.com/a/gitlab.com/file/d/1q8N-idoYGFSCw2ajat9RscfQ004EL-PS/view?usp=sharing) and email it to People Operations once completed.
1. [ ] New Team Member: If you don't have a BSN number you will need to apply for one asap. Details on that process can be found under the BSN number section on the [visas page in the handbook](https://about.gitlab.com/handbook/people-operations/visas/#bsn-number)
1. [ ] New Team Member: To get your internet subscription reimbursement, fill in and sign the [Regeling Internet Thuis form](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) and email it to People Ops. People Ops will then send it to the payroll provider in the Netherlands via email.

</details>

<details>
<summary>People Ops</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR is marked as `Probation Period` and add he end date exactly 1 month from the hire date. Also add a second status as `Temporary Contract Ending` with the end date as exactly one year from the hire date.
1. [ ] People Ops: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Ops: Send the payroll form and wage tax form to HRSavvy along with the contract of employment, copy of the residence permit (if applicable), copy of the debit card, and passport. Then file the form in BambooHR.
1. [ ] People Ops: If the position is in development or research, it likely
qualifies for [WBSO (R&D tax credit)](https://about.gitlab.com/handbook/people-operations/#wbso-rd-tax-credit-in-the-netherlands); add their info to the [WBSO sheet](https://docs.google.com/spreadsheets/d/1pNprxjsyJA45wGV38zGVyhotKWRJ99iQ8CvRZtz_H8w/edit?ts=5d2c3441#gid=0) for the Director of Tax to review and approve for the application. 
1. [ ] People Ops: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>