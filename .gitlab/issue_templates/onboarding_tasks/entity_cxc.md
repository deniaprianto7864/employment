### For employees with CXC only

**New Team Member**
1. [ ] New team member: you should have been contacted already by CXC to walk you through their onboarding for payroll. Please reach out to People Operations if you have not received any information or have not been contacted by CXC.
1. [ ] New team member: read through the [benefits section](https://about.gitlab.com/handbook/benefits/#CXC) for your location.

**People Ops**
1. [ ] People Ops: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

