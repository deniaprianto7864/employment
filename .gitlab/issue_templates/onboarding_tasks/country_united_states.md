### For employees in the US only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: If you have not done so before Day 1, make sure you have completed Section 1 of your I-9 in Fragomen (following instructions sent to your personal email address before your start date) by creating an account using the following link https://gitlab.i9servicecenter.com. Note - this is critical and must be completed on or before your first day of hire. You must contact People Ops if you have difficulty with this form. Send the name and agent of your Designated Agent for Section 2 to People Ops.
1. [ ] New team member (California residents only): Read and be aware of the [Victims of Domestic Violence Leave Notice](https://www.dir.ca.gov/dlse/Victims_of_Domestic_Violence_Leave_Notice.pdf?utm_campaign=17-Q3-AUG-ACC-PAS-SOI-CLIENT-CA%20DOMESTIC%20VIOLENCE%20EMPLOYMENT%20LEAVE%20ACT&utm_medium=email&utm_source=Eloqua&elqTrackId=d6bf3067231c4a75a25cfd2b11703199&elq=164aa89072ad408db913259dae224863&elqaid=10312&elqat=1&elqCampaignId=).

**To Do after I-9 is complete:**
1. [ ] New team member: Once you receive the login from ADP to your GitLab email address, update all information in ADP including Direct Deposit, W4 Withholdings, marital status, etc. Payroll will reach out to you prior to your first paycheck; don’t worry if this isn’t done on Day 1 as new Labbers are frequently added to ADP in waves coinciding with the ending of each pay period. To clarify, your invitation to ADP will arrive between Day 2-4 most probably.
1. [ ] New team member: Elect your [benefits through Lumity](https://about.gitlab.com/handbook/benefits/inc-benefits-us/). If you have any questions please reach out to People Operations. Please note, your invitation to Lumity will arrive between Day 2-4.
    1. For more information on the plan, please review the [Summary Plan Description](https://drive.google.com/file/d/1K8uybZ_pQc-XxpOaIEqnSN-UlvZCsf1W/view?usp=sharing). 
    1. Please ensure that when you are electing your benefits via Lumity that you are completing both your new hire enrollment for benefits through December 31, 2019 and open enrollment for benefits starting on January 1, 2020. 
1. [ ] New team member: Review [GitLab's 401(k)](https://about.gitlab.com/handbook/benefits/inc-benefits-us/#401k-plan) Plan. You will receive an email from Betterment within your first week. If you do not, please reach out to People Ops. 
   1. For more information on the plan, please review the [Summary Plan Description](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing).

</details>

<details>
<summary>People Ops</summary>

1. [ ] People Ops: If new team member is an hourly employee, create a timesheet, share with Finance and the new team member. Also, send an email to Finance to let them know that an hourly employee has started.

**To Do after I-9 is complete:**
1. [ ] People Ops (Analyst): 
     *  Once the BambooHR profile has been audited, check the "I-9 Processed" box located on the Personal tab in BambooHR.
     *  Confirm the BambooHR profile has been audited and the employee is ready to be added to ADP by tagging the Payroll Specialist in a comment.

</details>


<details>
<summary>Payroll</summary>

1. [ ] Payroll: Add to ADP. Ensure payroll is complete from start date if the payroll has already closed for those days.   
</details>
