#### Sales Division

**Sales Enablement**
1. [ ] Sales Enablement (@tparuchuri): Add new Sales Team Member to Sales Quickstart Learning Path in Google Classroom. New sales team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quickstart Learning Path. This Learning Path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

**Manager**
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/u/0/#shared-with-me) in our Google Docs.
1. [ ] Manager: Inform Sales Operations (@atkach1) what territory the new team member will be working and if they have a paired SDR.
1. [ ] Manager: Work with existing territory owner to define which opportunities will remain with the current owner. Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Slack Sales Operations (@atkach1) the new team member's quota information for upload into Salesforce

**Sales Operations & Strategy**
1. [ ] Sales Operations (@atkach1): Prepare MR to Handbook with new territory assignments.
1. [ ] Sales Operations (@atkach1): Create territory review report for Manager to review with current territory owner.
1. [ ] Sales Operations (@atkach1): Two weeks after start date, Update ownership of Accounts, Leads and Contacts in territory with additional feedback from RD meeting with previous territory owner.
1. [ ] Sales Operations (@atkach1): Two weeks after start date, update territories on user profiles.
1. [ ] Sales Operations (@atkach1): When adding to Salesforce ensure that they are added to the appropriate group that either grants or restricts access to Pub Sec Data
1. [ ] Sales Strategy (@Swetha): Set BCR, SCR (if applicable), and prorated quota and deliver participant plan/schedule
1. [ ] Sales Strategy (@Swetha): Deliver Participant Schedule.

**New Team Member**
1. [ ] New team member: Complete your Sales Quickstart Learning Path (see your email for an invitation from Google Classroom). If you have questions, please reach out to Tanuja (@tparuchuri).
1. [ ] New team member: Follow [these instructions](https://help.datafox.com/hc/en-us/articles/227081328-User-Setup-Connect-your-DataFox-User-to-your-Salesforce-Account) to link your DataFox Account with your Salesforce Account.
1. [ ] New team member: For all roles EXCEPT PubSec Inside Sales Rep and SMB Customer Advocate, please consult with your manager to determine whether or not you need access to Zendesk Light Agent. If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access. Note: SMB Customer Advocates are provisioned "GitLab Staff Role" access to Zendesk and PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal within their role-based access request template above. PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters.

**Marketing**
1. [ ] Marketing (@jjcordz): Two weeks after start date, go live with update to Territory Model in LeanData with new territory assignments.

**Sales Account Assignment**
1. [ ] Manager: Inform Sales Operations (@atkach1) what territory the new team member will be working and if they have a paired SDR
1. [ ] Manager: Notify Sales Operations (@atkach1) in a Slack DM the new team members quota information for upload into Salesforce.
1. [ ] **Two weeks after start date** Sales Operations (@atkach1) will assign new Sales Team member (SAL/AE/AM) all accounts, contacts and leads (if `Large`) in newly assigned territory.

