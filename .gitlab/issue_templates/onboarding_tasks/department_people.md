#### For People Ops Only

<details>
<summary>Manager</summary>

1. [ ] Manager: Add team member to HR Savvy as an admin (if applicable).

</details>


<details>
<summary>People Ops</summary>

1. [ ] People Ops: Add team member to BambooHR as an admin.
1. [ ] People Ops (@ewegscheider): Add team member to Greenhouse as "Job Admin: People Ops".
1. [ ] People Ops: Invite team member to People Ops Confidential channel in Slack.
2. [ ] People Ops: Ping `@tknudsen` or `@shaynes13` in this issue to add team member to the calendar invite for the PeopleOps team meetup at Contribute March 2020.

</details>