### For employees in the UK only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: if you do not have a National Insurance (NI) number please apply for one immediately following the instructions [on this website](https://www.gov.uk/apply-national-insurance-number). Once you have received your NI number please enter it into BambooHR, on the Personal tab of the My Info page.
1. [ ] New Team Member: Once you have received the payroll form, please fill in the missing details and sign. This form will then be sent electronically to GitLab's payroll provider Vistra. 
1. [ ] New Team Member: If you have a P45 from your previous company please email this to peopleops@gitlab.com. 
1. [ ] New Team Member: If you don't have a P45 then you must complete the [New Starter Checklist](https://public-online.hmrc.gov.uk/lc/content/xfaforms/profiles/forms.html?contentRoot=repository:///Applications/PersonalTax_A/1.0/SC2&template=SC2.xdp). When you have completed it, please save it as a pdf and email it to peopleops@gitlab.com.
1. [ ] New Team Member: Read through the [AXA PPP Brochure](https://drive.google.com/a/gitlab.com/file/d/0Bwy71gCp1WgtUXcxeFBaM0MyT00/view?usp=sharing) and let People Ops know if you would like to join the medical insurance scheme. You can find some more information on the [benefits section](https://about.gitlab.com/handbook/benefits/ltd-benefits-uk/#medical-insurance) also. This does not currently include dental or optical care. Please also note that this is a P11d taxable benefit.
1. [ ] New Team Member: Please read through the auto-enrolment personal pension details which you can find on the [pensions section](https://about.gitlab.com/handbook/benefits/ltd-benefits-uk/#pension-introduction) of the benefits page.

</details>

<details>
<summary>People Ops</summary>

Before Start Date
1. [ ] People Ops: A few days before new team member's start date, send new team member the [UK email](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/uk_email.md) with New Starter Checklist link, information about P45 and notification that UK payroll form will be sent from HelloSign.
1. [ ] People Ops: A few days before new team member's start date, using the HelloSign template, send new team member the UK payroll form. Fill in as much information as possible from BambooHR and stage in HelloSign for signature. Cc People Ops and Vistra (email address is located in 1Password Vault => Payroll Contacts). 
1. [ ] People Ops: Once the payroll form has been completed and signed by the new team member file the document in BambooHR under Documents/Payroll Forms. 
1. [ ] People Ops: If a New Starter Checklist was completed, save it in the same folder. If new team member has a P45, please save it there as well.
1. [ ] People Ops: When the Payroll form and P45 or New Starter Checklist are complete, send as encrypted attachments to Vistra along with an encrypted copy of the team member's signed contract. The signed contract is found in the Contracts & Changes folder in the team member's BambooHR profile. This information should not be sent later than Day 1 of a new hire's start date.

After Start Date
1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has two entries marked as `Probation Period` with date as the hire date and `probation period ends` with the end date as exactly 3 months from the hire date.
1. [ ] People Ops: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Ops: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>