### For employees in Belgium Only

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: To ensure that we have all of your details ready for payroll processing please complete the following information on BambooHR:
     1. [ ] Full name
     1. [ ] Date of birth
     1. [ ] Address
     1. [ ] National Number
     1. [ ] Bank details
1. [ ] New Team Member: Please read the Work Rules in either [Dutch](https://drive.google.com/file/d/1o_7sbqqxlxZW8PksXblgmbs8gi4yPrvb/view?usp=sharing) or [French](https://drive.google.com/file/d/13geOdZuMCE4XX8e5ejd5Qe5FAMnR1gWC/view?usp=sharing).

</details>


<details>
<summary>People Ops</summary>

1. [ ] People Ops: Reach out to new team member and ask them to confirm their marital status and number of children. This is required for tax purposes.
1. [ ] People Ops: Verify the new team member's legal name on photo ID matches the legal name entered in BambooHR.
1. [ ] People Ops: Once all the information has been entered by the new team member, send this to Vistra by email (details under secure notes on the 1pasword People Ops Vault) along with a:
     1. [ ] Scanned copy of employment contract
     1. [ ] ID card
     1. [ ] Email address
     1. [ ] Salary
1. [ ] People Ops: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>