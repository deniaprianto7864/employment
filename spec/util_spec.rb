# frozen_string_literal: true

require_relative 'spec_helper'

describe PeopleOps::Utils do
  describe 'date_of_next' do
    before do
      allow(Date).to receive(:parse).and_call_original
      allow(Date).to receive(:parse).with('Monday').and_return(Date.parse('2019-06-03'))
    end

    context 'when next instance is in the current week' do
      before do
        allow(Date).to receive(:today).and_return(Date.parse('2019-06-02'))
      end

      it 'returns correct date' do
        expect(described_class.date_of_next('Monday')).to eq(Date.parse('2019-06-03'))
      end
    end

    context 'when next instance is in the next week' do
      before do
        allow(Date).to receive(:today).and_return(Date.parse('2019-06-05'))
      end

      it 'returns correct date' do
        expect(described_class.date_of_next('Monday')).to eq(Date.parse('2019-06-10'))
      end
    end
  end

  describe 'date_of_prev' do
    before do
      allow(Date).to receive(:parse).and_call_original
      allow(Date).to receive(:parse).with('Monday').and_return(Date.parse('2019-06-03'))
    end

    context 'when next instance is in the current week' do
      before do
        allow(Date).to receive(:today).and_return(Date.parse('2019-06-05'))
      end

      it 'returns correct date' do
        expect(described_class.date_of_prev('Monday')).to eq(Date.parse('2019-06-03'))
      end
    end

    context 'when next instance is in the next week' do
      before do
        allow(Date).to receive(:today).and_return(Date.parse('2019-06-02'))
      end

      it 'returns correct date' do
        expect(described_class.date_of_prev('Monday')).to eq(Date.parse('2019-05-27'))
      end
    end
  end
end
