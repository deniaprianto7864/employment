# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Announcement::BasicAnnouncement do
  let(:bamboointerface_mock) { instance_double(PeopleOps::Interface::Bamboo) }
  let(:slackinterface_mock) { instance_double(PeopleOps::Interface::Slack, send_message: nil) }
  let(:gitlabinterface_mock) { instance_double(PeopleOps::Interface::GitLab) }
  let(:ba) { described_class.new }

  before do
    allow(PeopleOps::Interface::Bamboo).to receive(:new).and_return(bamboointerface_mock)
    allow(PeopleOps::Interface::Slack).to receive(:new).and_return(slackinterface_mock)
    allow(PeopleOps::Interface::GitLab).to receive(:new).and_return(gitlabinterface_mock)
  end

  describe '#bamboo' do
    it 'creates bamboo interface correctly' do
      expect(described_class.new.bamboo).to eq(bamboointerface_mock)
    end
  end

  describe '#slack' do
    it 'creates slack interface correctly' do
      expect(described_class.new.slack).to eq(slackinterface_mock)
    end
  end

  describe '#gitlab' do
    it 'creates gitlab interface correctly' do
      expect(described_class.new.gitlab).to eq(gitlabinterface_mock)
    end
  end

  describe '#channel' do
    context 'when left undefined' do
      it 'raises error' do
        expect { described_class.new.channel }.to raise_error(NotImplementedError)
      end
    end
  end

  describe '#message' do
    context 'when left undefined' do
      it 'raises error' do
        expect { described_class.new.message }.to raise_error(NotImplementedError)
      end
    end
  end

  describe '#announce' do
    before do
      allow(described_class).to receive(:new).and_return(ba)
      allow(ba).to receive(:channel).and_return('mychannel')
      allow(ba).to receive(:message).and_return('mymessage')
    end

    context 'when dry run' do
      before do
        allow(ENV).to receive(:[]).and_call_original
        allow(ENV).to receive(:[]).with('DRY_RUN').and_return(true)
      end

      it 'prints the message to stdout' do
        expect { described_class.new.announce }.to output("mymessage\n").to_stdout
      end
    end

    context 'when not dry run' do
      before do
        allow(ENV).to receive(:[]).and_call_original
        allow(ENV).to receive(:[]).with('DRY_RUN').and_return(nil)
      end

      context 'when channel not set' do
        before do
          allow(ba).to receive(:channel).and_return(nil)
        end

        it 'does not call slack' do
          described_class.new.announce
          expect(slackinterface_mock).not_to have_received(:send_message)
        end
      end

      context 'when message not set' do
        before do
          allow(ba).to receive(:message).and_return(nil)
        end

        it 'does not call slack' do
          described_class.new.announce
          expect(slackinterface_mock).not_to have_received(:send_message)
        end
      end

      context 'when message and channel set' do
        it 'does not call slack' do
          described_class.new.announce
          expect(slackinterface_mock).to have_received(:send_message).with('mychannel', 'mymessage')
        end
      end
    end
  end
end
