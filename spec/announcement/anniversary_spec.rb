# frozen_string_literal: true

require_relative '../spec_helper'

describe PeopleOps::Announcement::Anniversary do
  let(:json_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/anniversary.json'))) }
  let(:msgs) do
    [
      'Congratulations to all GitLab Team Members with Work Anniversaries this week (2019-06-30 to 2019-07-06). :gitlab: :tada: :dancingpenguin: :all_the_things: ',
      '• <@UXASFG12>: 5th',
      '• John Doe: 3rd',
      '• Armanda Fiore: 1st'
    ].join("\n")
  end
  let(:slack_reply) do
    {
      'user' => {
        'id' => 'UXASFG12'
      }
    }
  end
  let(:slack_mock) { instance_double(Slack::Web::Client, chat_postMessage: true, users_lookupByEmail: true) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: instance_double(Bamboozled::API::Report, custom: json_file)) }

  before do
    allow(Date).to receive(:today).and_return(Date.parse('2019-07-04'))
    allow(PeopleOps::Utils).to receive(:date_of_prev).with('Sunday').and_return(Date.parse('2019-06-30'))
    allow(PeopleOps::Utils).to receive(:date_of_next).with('Monday').and_return(Date.parse('2019-07-08'))
    allow(Slack::Web::Client).to receive(:new).and_return(slack_mock)
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
    allow(slack_mock).to receive(:users_lookupByEmail).with(email: 'estoneham@mycompany.com').and_return(slack_reply)
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('CELEBRATIONS_SLACK_CHANNEL').and_return('CELEBRATIONS_CHANNEL')
    allow(ENV).to receive(:[]).with('DRY_RUN').and_return(false)
  end

  describe '#channel' do
    it 'detects slack channel from env variable' do
      expect(described_class.new.channel).to eq('CELEBRATIONS_CHANNEL')
    end
  end

  describe '#employee_anniversaries' do
    it 'detects employees with anniversaries' do
      employees = [
        [json_file[3], 5],
        [json_file[0], 3],
        [json_file[1], 1]
      ]

      expect(described_class.new.employee_anniversaries).to eq(employees)
    end
  end

  describe '#message' do
    it 'generates correct slack message' do
      expect(described_class.new.message).to eq(msgs)
    end
  end
end
