# frozen_string_literal: true

require_relative '../spec_helper'

describe PeopleOps::Announcement::Joining do
  let(:json_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/joining.json'))) }
  let(:msgs) do
    [
      'Welcome our newest GitLab Team Members, who are starting next week! Send them some welcome emails or coffee chats! :tada: :gitlab: :coffee: :grin: ',
      '1. Armanda Fiore, afiore@mycompany.com, joining on 2019-07-01 as Backend Engineer',
      '2. John Doe, johndoe@mycompany.com, joining on 2019-07-03 as Distribution Engineer',
      '',
      'For a detailed breakdown and overview of our hiring progress over time, visit https://app.periscopedata.com/app/gitlab/503779'
    ].join("\n")
  end
  let(:slack_mock) { instance_double(Slack::Web::Client, chat_postMessage: true) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: instance_double(Bamboozled::API::Report, custom: json_file)) }

  before do
    allow(Date).to receive(:today).and_return(Date.parse('2019-06-30'))
    allow(Slack::Web::Client).to receive(:new).and_return(slack_mock)
    allow(PeopleOps::Utils).to receive(:date_of_prev).with('Sunday').and_return(Date.parse('2019-06-23'))
    allow(PeopleOps::Utils).to receive(:date_of_next).with('Sunday').and_return(Date.parse('2019-07-01'))
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('ANNOUNCEMENT_SLACK_CHANNEL').and_return('ANNOUNCEMENT_CHANNEL')
  end

  describe '#channel' do
    it 'detects slack channel from env variable' do
      expect(described_class.new.channel).to eq('ANNOUNCEMENT_CHANNEL')
    end
  end

  describe '#new_hires' do
    it 'detects employees joining next week' do
      employees = json_file[0, 2].sort_by { |emp| emp['hireDate'] }

      expect(described_class.new.new_hires).to eq(employees)
    end
  end

  describe '#message' do
    it 'generates correct slack message' do
      expect(described_class.new.message).to eq(msgs)
    end
  end
end
