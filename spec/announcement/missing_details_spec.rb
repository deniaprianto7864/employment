# frozen_string_literal: true

require_relative '../spec_helper'

describe PeopleOps::Announcement::MissingDetails do
  let(:json_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/missing_information.json'))) }
  let(:msgs) do
    [
      'The following team members will be joining next week, but have incomplete BambooHR profile. Please take a look. ',
      '1. John Doe: https://gitlab.bamboohr.com/employees/employee.php?id=448400. Missing fields are department',
      '2. Armanda Fiore: https://gitlab.bamboohr.com/employees/employee.php?id=448401. Missing fields are department, division',
      '3. Evan Stoneham: https://gitlab.bamboohr.com/employees/employee.php?id=448403. Missing fields are country',
      '4. Latanya Wilbert: https://gitlab.bamboohr.com/employees/employee.php?id=448404. Missing fields are workEmail'
    ].join("\n")
  end
  let(:slack_mock) { instance_double(Slack::Web::Client, chat_postMessage: true) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: instance_double(Bamboozled::API::Report, custom: json_file)) }

  before do
    allow(Date).to receive(:today).and_return(Date.parse('2019-06-30'))
    allow(PeopleOps::Utils).to receive(:date_of_next).with('Sunday').and_return(Date.parse('2019-07-01'))
    allow(Slack::Web::Client).to receive(:new).and_return(slack_mock)
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('REMINDER_SLACK_CHANNEL').and_return('REMINDER_CHANNEL')
  end

  describe '#channel' do
    it 'detects slack channel from env variable' do
      expect(described_class.new.channel).to eq('REMINDER_CHANNEL')
    end
  end

  describe '#missing_details_employees' do
    it 'detects employees with missing details' do
      missing_employees = [
        [json_file[0], ['department']],
        [json_file[1], %w[department division]],
        [json_file[3], ['country']],
        [json_file[4], ['workEmail']]
      ]

      expect(described_class.new.missing_details_employees).to eq(missing_employees)
    end
  end

  describe '#message' do
    context 'when there are new hires with missing details' do
      it 'generates correct slack message' do
        expect(described_class.new.message).to eq(msgs)
      end
    end

    context 'when there are not new hires with missing details' do
      before do
        class_mock = described_class.new
        allow(described_class).to receive(:new).and_return(class_mock)
        allow(class_mock).to receive(:missing_details_employees).and_return({})
      end

      it 'generates correct slack message' do
        expect(described_class.new.message).to eq('Woohoo. BambooHR profile of all team members joining next week are complete. :dancing_banana: ')
      end
    end
  end
end
