# frozen_string_literal: true

require 'simplecov'

SimpleCov.start

Dir[File.join(__dir__, '../lib/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  Encoding.default_external = 'UTF-8'
end
