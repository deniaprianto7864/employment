# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Interface::GitLab do
  let(:gitlab_members) { spy }
  let(:gitlab_mock) { instance_double(Gitlab::Client, group_members: gitlab_members, create_issue: true, issues: double(auto_paginate: true), create_issue_note: true) }

  before do
    allow(Gitlab).to receive(:client).and_return(gitlab_mock)
  end

  describe '#find_gitlabber' do
    context 'when given empty query' do
      it 'returns nil' do
        expect(described_class.new.find_gitlabber(nil)).to be_nil
      end
    end

    context 'when given non-empty query' do
      it 'calls correct method with proper args' do
        described_class.new.find_gitlabber('john')

        expect(gitlab_mock).to have_received(:group_members).with('gitlab-com', query: 'john')
        expect(gitlab_members).to have_received(:first)
      end
    end
  end

  describe '#create_issue' do
    it 'calls correct method with proper args' do
      described_class.new.create_issue('foo/bar', 'My Issue Title', assignee_id: 123_456)
      expect(gitlab_mock).to have_received(:create_issue).with('foo/bar', 'My Issue Title', assignee_id: 123_456)
    end
  end

  describe '#get_onboarding_issues' do
    it 'uses specified arguments' do
      described_class.new.get_onboarding_issues('foo/bar', per_page: 500)

      expect(gitlab_mock).to have_received(:issues).with('foo/bar', per_page: 500)
    end
  end

  describe '#create_issue_note' do
    it 'calls correct method with proper args' do
      described_class.new.create_issue_note('foo/bar', 15, 'My issue comment')

      expect(gitlab_mock).to have_received(:create_issue_note).with('foo/bar', 15, 'My issue comment')
    end
  end
end
