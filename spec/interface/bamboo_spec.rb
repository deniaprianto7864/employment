# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Interface::Bamboo do
  let(:json_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/joining.json'))) }
  let(:bamboo_employee_mock) { instance_double(Bamboozled::API::Employee, find: true) }
  let(:bamboo_mock) do
    instance_double(Bamboozled::Base,
                    report: instance_double(Bamboozled::API::Report, custom: json_file),
                    employee: bamboo_employee_mock)
  end

  before do
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
  end

  describe '#get_employee_details' do
    it 'finds employee correctly' do
      expect(described_class.new.get_employee_details('448400')).to eq(json_file[0])
    end
  end

  describe '#get_employee' do
    it 'calls correct API method' do
      described_class.new.get_employee('448400')

      expect(bamboo_employee_mock).to have_received(:find).with('448400', %w[firstName lastName jobTitle supervisor hireDate country location department division workEmail])
    end

    describe '#search_employee' do
      it 'returns an employee from displayName' do
        expect(described_class.new.search_employee('John Doe')).to eq(json_file[0])
      end

      it 'returns an employee from firstname and lastname' do
        expect(described_class.new.search_employee('Theodore Smith')).to eq(json_file[6])
      end
    end
  end
end
