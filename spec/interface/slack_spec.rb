# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Interface::Slack do
  let(:slack_mock) { instance_double(::Slack::Web::Client, chat_postMessage: true, users_lookupByEmail: 'Random User') }

  before do
    allow(::Slack::Web::Client).to receive(:new).and_return(slack_mock)
  end

  describe '#send_message' do
    it 'calls correct slack API method' do
      described_class.new.send_message('foo', 'bar')

      expect(slack_mock).to have_received(:chat_postMessage).with(channel: 'foo', text: 'bar', as_user: true)
    end
  end

  describe '#find_user' do
    context 'when user with email id exists' do
      it 'calls correct slack API method' do
        expect(described_class.new.find_user('foo@example.com')).to eq('Random User')

        expect(slack_mock).to have_received(:users_lookupByEmail).with(email: 'foo@example.com')
      end
    end

    context 'when user with email id does not exists' do
      it 'calls correct slack API method' do
        allow(slack_mock).to receive(:users_lookupByEmail).and_raise(::Slack::Web::Api::Errors::SlackError, 'users_not_found')

        expect(described_class.new.find_user('foo@example.com')).to eq(nil)

        expect(slack_mock).to have_received(:users_lookupByEmail).with(email: 'foo@example.com')
      end
    end
  end
end
