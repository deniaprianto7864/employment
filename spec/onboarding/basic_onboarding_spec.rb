# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Onboarding::BasicOnboarding do
  let(:json_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/joining.json'))) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: instance_double(Bamboozled::API::Report, custom: json_file)) }
  let(:gitlabinterface_mock) { instance_double(PeopleOps::Interface::GitLab, find_gitlabber: nil) }
  let(:oi) { described_class.new }

  before do
    allow(Date).to receive(:today).and_return(Date.parse('2019-06-30'))
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
    allow(ENV).to receive(:[]).and_call_original
    allow(oi).to receive(:gitlab).and_return(gitlabinterface_mock)
  end

  describe '#project_path' do
    context 'with EMPLOYMENT_PROJECT_PATH variable set' do
      before do
        allow(ENV).to receive(:[]).and_call_original
        allow(ENV).to receive(:[]).with('EMPLOYMENT_PROJECT_PATH').and_return('foo')
      end

      it 'sets the variable value as project_path' do
        expect(described_class.new.project_path).to eq('foo')
      end
    end

    context 'with EMPLOYMENT_PROJECT_PATH variable unset and CI_PROJECT_PATH variable set' do
      before do
        allow(ENV).to receive(:[]).and_call_original
        allow(ENV).to receive(:[]).with('EMPLOYMENT_PROJECT_PATH').and_return(nil)
        allow(ENV).to receive(:[]).with('CI_PROJECT_PATH').and_return('bar')
      end

      it 'sets the variable value as project_path' do
        expect(described_class.new.project_path).to eq('bar')
      end
    end
  end

  describe '#bamboo' do
    it 'creates gitlab interface correctly' do
      allow(PeopleOps::Interface::Bamboo).to receive(:new).and_return(true)
      described_class.new.bamboo

      expect(PeopleOps::Interface::Bamboo).to have_received(:new)
    end
  end

  describe '#gitlab' do
    it 'creates gitlab interface correctly' do
      allow(PeopleOps::Interface::GitLab).to receive(:new).and_return(true)
      described_class.new.gitlab

      expect(PeopleOps::Interface::GitLab).to have_received(:new)
    end
  end

  describe '#bamboo_id=' do
    it 'sets the value' do
      oi.bamboo_id = 5

      expect(oi.bamboo_id).to eq(5)
    end
  end

  describe '#populate_employee_details' do
    it 'do not set manager if supervisorEid not found' do
      oi.bamboo_id = '448406'
      oi.populate_employee_details

      expect(oi.manager).to be_nil
    end

    it 'set manager from supervisor name' do
      allow(gitlabinterface_mock).to receive(:find_gitlabber).with('John Doe').and_return(json_file[0])

      oi.bamboo_id = '448401'
      oi.populate_employee_details

      expect(oi.manager['displayName']).to eq('John Doe')
    end

    it 'set manager from supervisor email' do
      allow(gitlabinterface_mock).to receive(:find_gitlabber).with('John Doe').and_return(nil)
      allow(gitlabinterface_mock).to receive(:find_gitlabber).with('johndoe@mycompany.com').and_return(json_file[0])

      oi.bamboo_id = '448401'
      oi.populate_employee_details

      expect(oi.manager['displayName']).to eq('John Doe')
    end
  end
end
