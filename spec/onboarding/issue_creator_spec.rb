# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Onboarding::IssueCreator do
  let(:ic) { described_class.new }
  let(:description) { "Welcome to your onboarding issue.\nManager is `__MANAGER_HANDLE__`.\n1. [ ] Sample task" }
  let(:json_file) { JSON.parse(File.read(File.join(__dir__, '../fixtures/joining.json'))) }
  let(:bamboo_mock) { instance_double(Bamboozled::Base, report: instance_double(Bamboozled::API::Report, custom: json_file)) }
  let(:modified_description) { "Welcome to your onboarding issue.\nManager is @johndoe. PeopleOps is @peopleopsuser.\n1. [ ] Sample task" }
  let(:manager_gitlab) { double(id: 1, username: 'johndoe', web_url: nil) }
  let(:gitlabinterface_mock) { instance_double(PeopleOps::Interface::GitLab, create_issue: double(web_url: 'https://foo.com/blah')) }

  before do
    allow(described_class).to receive(:new).and_return(ic)
    allow(ic).to receive(:project_path).and_return('myrandompath')
    allow(Bamboozled).to receive(:client).and_return(bamboo_mock)
    allow(ic).to receive(:gitlab).and_return(gitlabinterface_mock)
    allow(gitlabinterface_mock).to receive(:find_gitlabber).with('John Doe').and_return(manager_gitlab)
  end

  describe '#generate_description' do
    it 'detects the user who ran the command' do
      allow(ENV).to receive(:[]).and_call_original
      allow(ENV).to receive(:key?).with('GITLAB_USER_LOGIN').and_return(true)
      allow(ENV).to receive(:[]).with('GITLAB_USER_LOGIN').and_return('peopleopsuser')
      expect(described_class.new.generate_description).to match(/People Ops is tackled by @peopleopsuser/)
    end

    context 'when manager was found' do
      it 'replaces manager username correctly' do
        ic.manager = manager_gitlab
        expect(described_class.new.generate_description).to match(/Manager is accurate and confirm it is @johndoe/)
      end
    end

    context 'when no manager was found' do
      it 'keeps manager placeholder' do
        expect(described_class.new.generate_description).to match(/__MANAGER_HANDLE__/)
      end
    end
  end

  describe '#create_issue' do
    before do
      allow(File).to receive(:read).and_call_original
      allow(File).to receive(:read).with('.gitlab/issue_templates/onboarding.md').and_return(description)
      allow(ic).to receive(:generate_description).and_return(modified_description)
    end

    context 'when DRY_RUN' do
      before do
        allow(ENV).to receive(:[]).and_call_original
        allow(ENV).to receive(:[]).with('DRY_RUN').and_return(true)
        ic.description = modified_description
      end

      it 'prints the description' do
        expect { described_class.new.create_issue('448401') }.to output(/Armanda Fiore's onboarding, starting on 2019-07-01 as Backend Engineer/).to_stdout
      end
    end

    context 'when not DRY_RUN' do
      before do
        allow(ENV).to receive(:[]).and_call_original
        allow(ENV).to receive(:[]).with('DRY_RUN').and_return(false)
        allow(ENV).to receive(:[]).with('GITLAB_USER_ID').and_return(1234)
        allow(Date).to receive(:today).and_return(Date.parse('2019-07-05'))
        ic.description = modified_description
      end

      it 'prints the issue link' do
        final_description = "<!-- BAMBOOHR_ID: 448401 -->\n" + modified_description
        args = {
          description: final_description,
          labels: ['onboarding'],
          confidential: true,
          assignee_ids: [1234, 1],
          due_date: '2019-08-09'
        }

        expect { described_class.new.create_issue('448401') }.to output(%r{Onboarding issue opened at https://foo.com/blah}).to_stdout
        expect(gitlabinterface_mock).to have_received(:create_issue).with('myrandompath', "Armanda Fiore's onboarding, starting on 2019-07-01 as Backend Engineer", args)
      end
    end
  end
end
