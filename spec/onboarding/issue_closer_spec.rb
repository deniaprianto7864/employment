# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Onboarding::IssueCloser do
  let(:closer) { described_class.new }
  let(:onboarding_issues) do
    [
      double(iid: 1, web_url: 'https://example.com/issues/1', title: 'Onboarding John Doe starting on 2019-07-03 as Distribution Engineer', description: 'BAMBOOHR_ID: 448400', created_at: '2019-05-03'),
      double(iid: 2, web_url: 'https://example.com/issues/2', title: 'Onboarding Armanda Fiore starting on 2019-07-01 as Backend Engineer', description: 'BAMBOOHR_ID: 448401', created_at: '2019-05-03'),
      double(iid: 3, web_url: 'https://example.com/issues/3', title: 'Onboarding Akilah Welsh starting on 2019-07-08 as Frontend Engineer', description: 'Something else', created_at: '2019-05-03'),
      double(iid: 4, web_url: 'https://example.com/issues/4', title: 'Onboarding Evan Stoneham starting on 2019-06-25 as Product Manager', description: 'BAMBOOHR_ID: 448403', created_at: '2019-05-03'),
      double(iid: 5, web_url: 'https://example.com/issues/5', title: 'Onboarding Latanya Wilbert starting on 2015-07-03 as Test Automation Engineer', description: 'BAMBOOHR_ID: 448404', created_at: '2019-05-03'),
      double(iid: 6, web_url: 'https://example.com/issues/6', title: 'Onboarding Jane Doe starting on as Distribution Engineer', description: 'BAMBOOHR_ID: 448405', created_at: '2019-05-03'),
      double(iid: 7, web_url: 'https://example.com/issues/7', title: 'Onboarding Ted Smith starting on as Distribution Engineer', description: 'BAMBOOHR_ID: 448406', created_at: '2019-05-03')
    ]
  end
  let(:gitlabinterface_mock) { instance_double(PeopleOps::Interface::GitLab, get_onboarding_issues: onboarding_issues, create_issue_note: true) }

  before do
    allow(closer).to receive(:gitlab).and_return(gitlabinterface_mock)
    allow(closer).to receive(:project_path).and_return('myrandompath')
    allow(Date).to receive(:today).and_return(Date.parse('2019-07-04'))
    allow(described_class).to receive(:new).and_return(closer)
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('DRY_RUN').and_return(false)
  end

  describe '#outdated_issues' do
    it 'calls method with proper arguments' do
      args = {
        per_page: 100,
        labels: ['onboarding'],
        confidential: true,
        state: 'opened',
        created_before: Date.parse('2019-05-05'),
        created_after: Date.parse('2019-04-28')
      }

      described_class.new.outdated_issues

      expect(gitlabinterface_mock).to have_received(:get_onboarding_issues).with('myrandompath', args)
    end
  end

  describe '#close_outdated_issues' do
    it 'creates issue note' do
      close_message = <<~MSG
        This onboarding issue has been open for more than 60 days. It is being automatically closed to de-clutter this issue tracker.

        If you have any onboarding tasks remaining, reopen the issue and ping your Manager and PeopleOps to get them completed as soon as possible.

        /close
      MSG

      described_class.new.close_outdated_issues

      (1..7).each do |iid|
        expect(gitlabinterface_mock).to have_received(:create_issue_note).with('myrandompath', iid, close_message).once
      end
    end
  end
end
