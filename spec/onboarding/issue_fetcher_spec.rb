# frozen_string_literal: true

require_relative '../spec_helper.rb'

describe PeopleOps::Onboarding::IssueFetcher do
  let(:fetcher) { described_class.new }
  let(:onboarding_issues) do
    [
      double(web_url: 'https://example.com/issues/1', title: 'Onboarding John Doe starting on 2019-07-03 as Distribution Engineer', description: 'BAMBOOHR_ID: 448400'),
      double(web_url: 'https://example.com/issues/2', title: 'Onboarding Armanda Fiore starting on 2019-07-01 as Backend Engineer', description: 'BAMBOOHR_ID: 448401'),
      double(web_url: 'https://example.com/issues/3', title: 'Onboarding Akilah Welsh starting on 2019-07-08 as Frontend Engineer', description: 'Something else'),
      double(web_url: 'https://example.com/issues/4', title: 'Onboarding Evan Stoneham starting on 2019-06-25 as Product Manager', description: 'BAMBOOHR_ID: 448403'),
      double(web_url: 'https://example.com/issues/5', title: 'Onboarding Latanya Wilbert starting on 2015-07-03 as Test Automation Engineer', description: 'BAMBOOHR_ID: 448404'),
      double(web_url: 'https://example.com/issues/6', title: 'Onboarding Jane Doe starting on as Distribution Engineer', description: 'BAMBOOHR_ID: 448405'),
      double(web_url: 'https://example.com/issues/7', title: 'Onboarding Ted Smith starting on as Distribution Engineer', description: 'BAMBOOHR_ID: 448406'),
      double(web_url: 'https://example.com/issues/11', title: "Bob Tanuki's onboarding, starting on 2019-07-04 as Distribution Engineer", description: 'BAMBOOHR_ID: 448402')
    ]
  end
  let(:gitlabinterface_mock) { instance_double(PeopleOps::Interface::GitLab, get_onboarding_issues: onboarding_issues) }

  before do
    allow(fetcher).to receive(:gitlab).and_return(gitlabinterface_mock)
    allow(described_class).to receive(:new).and_return(fetcher)
  end

  describe '#list_new_hires' do
    context 'when a single date is specified' do
      it 'returns correct list of issues' do
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-07'),
          date: Date.parse('2019-07-03')
        }

        expect(described_class.new.list_new_hires(options)).to eq(
          [
            {
              bamboo_id: '448400',
              name: 'John Doe',
              date: Date.parse('2019-07-03'),
              issue_url: 'https://example.com/issues/1',
              job: 'Distribution Engineer'
            }
          ]
        )
      end
    end

    context 'when a range of dates are specified' do
      it 'returns correct list of issues in correct order' do
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-07'),
          date: nil
        }

        expect(described_class.new.list_new_hires(options)).to eq(
          [
            {
              bamboo_id: '448401',
              name: 'Armanda Fiore',
              date: Date.parse('2019-07-01'),
              issue_url: 'https://example.com/issues/2',
              job: 'Backend Engineer'
            },
            {
              bamboo_id: '448400',
              name: 'John Doe',
              date: Date.parse('2019-07-03'),
              issue_url: 'https://example.com/issues/1',
              job: 'Distribution Engineer'
            },
            {
              bamboo_id: '448402',
              name: 'Bob Tanuki',
              date: Date.parse('2019-07-04'),
              issue_url: 'https://example.com/issues/11',
              job: 'Distribution Engineer'
            }
          ]
        )
      end
    end
  end

  describe '#print_new_hire_list' do
    context 'when new hire list is empty' do
      before do
        allow(fetcher).to receive(:list_new_hires).and_return([])
      end

      it 'prints no new hire message' do
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-07'),
          date: Date.parse('2019-07-24')
        }
        expect { described_class.new.print_new_hire_list(options) }.to output(/Nope. No one is starting on dates matching specified criteria/).to_stdout
      end
    end

    context 'when new hires exist' do
      before do
        allow(fetcher).to receive(:list_new_hires).and_return(
          [
            {
              bamboo_id: '448401',
              name: 'Armanda Fiore',
              date: Date.parse('2019-07-01'),
              issue_url: 'https://example.com/issues/2',
              job: 'Backend Engineer'
            },
            {
              bamboo_id: '448400',
              name: 'John Doe',
              date: Date.parse('2019-07-03'),
              issue_url: 'https://example.com/issues/1',
              job: 'Distribution Engineer'
            }
          ]
        )
      end

      it 'prints proper new hire message' do
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-07'),
          date: nil
        }
        output_msg = "\n1. Armanda Fiore, 2019-07-01, Backend Engineer, https://example.com/issues/2, 448401\n2. John Doe, 2019-07-03, Distribution Engineer, https://example.com/issues/1, 448400\n"

        expect { described_class.new.print_new_hire_list(options) }.to output(output_msg).to_stdout
      end
    end
  end
end
