# frozen_string_literal: true

require_relative 'spec_helper'

describe PeopleOps do
  before do
    allow(Time).to receive(:now).and_return(Time.new('2019-06-27 15:24:32 +0530'))
  end

  describe '#run' do
    it 'triggers correct command' do
      allow(described_class).to receive(:send).and_return(true)

      described_class.run('onboarding', '40634')

      expect(described_class).to have_received(:send).with('onboarding', '40634')
    end
  end

  describe '#onboarding' do
    it 'triggers correct method' do
      onboarding_stub = instance_double(PeopleOps::Onboarding::IssueCreator, create_issue: true)
      allow(PeopleOps::Onboarding::IssueCreator).to receive(:new).and_return(onboarding_stub)

      described_class.onboarding('40634')

      expect(onboarding_stub).to have_received(:create_issue).with('40634')
    end
  end

  describe '#restricted_output' do
    it 'wraps the block output in a trace section' do
      expect { described_class.restricted_output { 'foo' } }.to output(/section_start/).to_stdout
      expect { described_class.restricted_output { 'foo' } }.to output(/section_end/).to_stdout
    end
  end

  describe '#listnewhires' do
    let(:list_hire_stub) { instance_double(PeopleOps::Onboarding::IssueFetcher, print_new_hire_list: true) }

    before do
      allow(PeopleOps::Onboarding::IssueFetcher).to receive(:new).and_return(list_hire_stub)
      allow(Date).to receive(:today).and_return(Date.parse('2019-06-30'))
    end

    context 'when from_date is specified' do
      it 'triggers correct method with proper arguments' do
        args = '-f 2019-06-30'
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-07'),
          date: nil
        }

        described_class.listnewhires(args)

        expect(list_hire_stub).to have_received(:print_new_hire_list).with(options)
      end
    end

    context 'when from_date and to_date are specified' do
      it 'triggers correct method with proper arguments' do
        args = '-f 2019-06-30 -t 2019-07-30'
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-30'),
          date: nil
        }

        described_class.listnewhires(args)

        expect(list_hire_stub).to have_received(:print_new_hire_list).with(options)
      end
    end

    context 'when date is specified' do
      it 'triggers correct method with proper arguments' do
        args = '-d 2019-07-15'
        options = {
          from_date: Date.parse('2019-06-30'),
          to_date: Date.parse('2019-07-07'),
          date: Date.parse('2019-07-15')
        }

        described_class.listnewhires(args)

        expect(list_hire_stub).to have_received(:print_new_hire_list).with(options)
      end
    end
  end

  describe '#joiningannoucnements' do
    it 'triggers correct method' do
      joining_stub = instance_double(PeopleOps::Announcement::Joining, announce: true)
      allow(PeopleOps::Announcement::Joining).to receive(:new).and_return(joining_stub)

      described_class.joiningannouncement('')

      expect(joining_stub).to have_received(:announce)
    end
  end

  describe '#anniversaryannouncement' do
    it 'triggers correct method' do
      anniversary_stub = instance_double(PeopleOps::Announcement::Anniversary, announce: true)
      allow(PeopleOps::Announcement::Anniversary).to receive(:new).and_return(anniversary_stub)

      described_class.anniversaryannouncement('')

      expect(anniversary_stub).to have_received(:announce)
    end
  end

  describe '#missingdetailsreminder' do
    it 'triggers correct method' do
      missing_details_stub = instance_double(PeopleOps::Announcement::MissingDetails, announce: true)
      allow(PeopleOps::Announcement::MissingDetails).to receive(:new).and_return(missing_details_stub)

      described_class.missingdetailsreminder('')

      expect(missing_details_stub).to have_received(:announce)
    end
  end

  describe '#closeoutdatedissues' do
    it 'triggers correct method' do
      close_stub = instance_double(PeopleOps::Onboarding::IssueCloser, close_outdated_issues: true)
      allow(PeopleOps::Onboarding::IssueCloser).to receive(:new).and_return(close_stub)

      described_class.closeoutdatedissues('')

      expect(close_stub).to have_received(:close_outdated_issues)
    end
  end
end
